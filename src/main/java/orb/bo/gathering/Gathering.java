package orb.bo.gathering;

import orb.bo.graph.Edge;

import java.util.ArrayList;
import java.util.List;

public class Gathering {
  public List<String> friends;
  public List<Edge<String>> relations;

  public Gathering() {
    this.friends = new ArrayList<>();
    this.relations = new ArrayList<>();
  }

  public List<String> getFriends() {
    return friends;
  }

  public void setFriends(List<String> friends) {
    this.friends = friends;
  }

  public List<Edge<String>> getRelations() {
    return relations;
  }

  public void setRelations(List<Edge<String>> relations) {
    this.relations = relations;
  }
}
