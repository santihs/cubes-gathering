package orb.bo;


import orb.bo.graph.Edge;
import orb.bo.services.ServiceFile;
import orb.bo.smt.Kruskal;
import orb.bo.smt.KruskalM;
import orb.bo.smt.KruskalX;
import org.jgrapht.Graph;
import org.jgrapht.graph.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Main {
  private static final String PATH = "./friends.txt";

  public static void main(String[] args) {
    Scanner read = new Scanner(System.in);

    if (System.getProperty("ONLINE_JUDGE") == null) {
      try {
        System.setOut(new PrintStream(
                new FileOutputStream("output.txt")));

        read = new Scanner(new File("input.txt"));
      } catch (Exception e) {
      }
    }

    int t = 1;
    // t = Integer.parseInt(read.nextLine());
    while (t > 0) {
      solve(read);
      t--;
    }
  }

  private static void solve(Scanner read) {
    String option = read.next();
    read.next();
    int m = read.nextInt();

    Graph<String, DefaultWeightedEdge> graph = fillGraph();

    Kruskal kruskal;
    if (option.equals("x")) {
      kruskal = new KruskalX();
      kruskal.mstKruskal(graph, m);
    } else if (option.equals("m")) {
      kruskal = new KruskalM();
      kruskal.mstKruskal(graph, m);
    }
  }

  private static Graph<String, DefaultWeightedEdge> fillGraph() {

    Graph<String, DefaultWeightedEdge> graph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

    ServiceFile service = new ServiceFile();
    var gathering = service.readFile(PATH);

    for (String friend : gathering.getFriends()) {
      graph.addVertex(friend);
    }

    for (Edge<String> relation : gathering.getRelations()) {
      var edge = graph.addEdge(relation.getSrc(), relation.getDest());
      graph.setEdgeWeight(edge, relation.getWeight());
    }
//    System.out.println(graph);
    return graph;
  }

}
