package orb.bo.services;

import orb.bo.gathering.Gathering;
import orb.bo.graph.Edge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ServiceFile {
  public Gathering readFile(String path) {
    Gathering gathering = new Gathering();

    File file = new File(path);
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      String line = "";
      while ((line = reader.readLine()) != null) {
        String[] edge = line.split(" ");
        if (edge.length == 1) {
          gathering.getFriends().add(line);
        } else if (edge.length == 3) {
          String src = edge[0];
          String dest = edge[1];
          Double weight = Double.valueOf(edge[2]);
          gathering.getRelations().add(new Edge<>(src, dest, weight));
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return gathering;
  }
}
