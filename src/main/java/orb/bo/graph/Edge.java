package orb.bo.graph;

import java.util.Objects;

public class Edge<T> implements Comparable<Edge<T>> {
  private T src;
  private T dest;
  private Double weight;

  public Edge(T src, T dest, Double weight) {
    this.src = src;
    this.dest = dest;
    this.weight = weight;
  }

  public T getSrc() {
    return src;
  }

  public void setSrc(T src) {
    this.src = src;
  }

  public T getDest() {
    return dest;
  }

  public void setDest(T dest) {
    this.dest = dest;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  @Override
  public int compareTo(Edge<T> o) {
    return weight.compareTo(o.weight);
  }
}