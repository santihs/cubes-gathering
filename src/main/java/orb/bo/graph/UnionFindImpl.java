package orb.bo.graph;

import org.jgrapht.alg.util.UnionFind;

import java.util.*;

public class UnionFindImpl<T> extends UnionFind<T> {

  /**
   * Creates a UnionFind instance with all the elements in separate sets.
   *
   * @param elements the initial elements to include (each element in a singleton set).
   */
  public UnionFindImpl(Set<T> elements) {
    super(elements);
  }


  public Map<T, T> getParentMap() {
    return super.getParentMap();
  }

  public Map<T, Integer> getRankMap() {
    return super.getRankMap();
  }

  public Map<T, Set<T>> getSetMap() { // O(N*log N)
    Map<T, Set<T>> setRep = new LinkedHashMap<>();
    for (T t : super.getParentMap().keySet()) { // O(N*log N)
      T representative = find(t); // O(log N)
      if (!setRep.containsKey(representative))
        setRep.put(representative, new LinkedHashSet<>());
      setRep.get(representative).add(t);
    }
    return setRep;
  }
}
