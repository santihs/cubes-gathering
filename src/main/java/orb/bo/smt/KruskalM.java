package orb.bo.smt;

import orb.bo.graph.UnionFindImpl;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.util.List;

public class KruskalM implements Kruskal {
  @Override
  public void mstKruskal(Graph<String, DefaultWeightedEdge> graph, int m) { // O(N*log N)

    if (m > graph.vertexSet().size()) {
      System.out.println("It is not possible");
      return;
    }

    Graph<String, DefaultWeightedEdge> mst = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

    List<DefaultWeightedEdge> sorted = createSortedEdges(graph); // O(N*log N)

    UnionFindImpl<String> ds = new UnionFindImpl<>(graph.vertexSet());

    for (DefaultWeightedEdge edge : sorted) { // O(N*log N)
      String source = graph.getEdgeSource(edge);
      String dest = graph.getEdgeTarget(edge);

      if (ds.numberOfSets() > m) {
        if (!ds.find(source).equals(ds.find(dest))) {
          mst.addVertex(source);
          mst.addVertex(dest);

          mst.setEdgeWeight(mst.addEdge(source, dest), graph.getEdgeWeight(edge));
          ds.union(source, dest); // O(log N)
        }
      }
    }

    printAnswer(mst, ds);
  }

  private static void printAnswer(Graph<String, DefaultWeightedEdge> mst, UnionFindImpl<String> ds) {
    var setMap = ds.getSetMap(); // O(N*log N)

    double total = Double.MAX_VALUE;
    String min = "";
    for (String key : setMap.keySet()) {
      String vertexes = String.join(" ", setMap.get(key));
      System.out.println(vertexes);
      if (setMap.get(key).size() > 1) {
        var weights = mst.incomingEdgesOf(key);
        double totalAux = 0;
        for (DefaultWeightedEdge weight : weights) {
          totalAux += mst.getEdgeWeight(weight);
        }
        if (totalAux < total) {
          total = totalAux;
          min = vertexes;
        }
      }
    }
    if (!min.isBlank()) {
      System.out.println("Least friendly relationship: " + min);
    }
  }

}
