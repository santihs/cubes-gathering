package orb.bo.smt;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.ArrayList;
import java.util.List;

public interface Kruskal {
  void mstKruskal(Graph<String, DefaultWeightedEdge> graph, int n);

  // O(N*log N)
  default List<DefaultWeightedEdge> createSortedEdges(Graph<String, DefaultWeightedEdge> graph) {
    List<DefaultWeightedEdge> sorted = new ArrayList<>(graph.edgeSet());
    sorted.sort((o1, o2) -> {
      Double f = graph.getEdgeWeight(o1);
      Double s = graph.getEdgeWeight(o2);
      return s.compareTo(f);
    });
    return sorted;
  }
}
