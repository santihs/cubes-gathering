package orb.bo.smt;

import orb.bo.graph.UnionFindImpl;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.util.*;
import java.util.stream.Collectors;

public class KruskalX implements Kruskal {

  public void mstKruskal(Graph<String, DefaultWeightedEdge> graph, int x) { // O(N^2 + N*M)
    Graph<String, DefaultWeightedEdge> mst = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);

    List<DefaultWeightedEdge> sorted = createSortedEdges(graph); // O(N*log N)

    Set<String> elements = new HashSet<>();
    for (DefaultWeightedEdge edge : sorted) { // O(N)
      if (graph.getEdgeWeight(edge) > x) {
        elements.add(graph.getEdgeSource(edge));
        elements.add(graph.getEdgeTarget(edge));
      }
    }

    UnionFindImpl<String> ds = new UnionFindImpl<>(elements);

    for (DefaultWeightedEdge edge : sorted) { // O(N*log N)
      String source = graph.getEdgeSource(edge);
      String dest = graph.getEdgeTarget(edge);
      var weight = graph.getEdgeWeight(edge);

      if (weight > x && !ds.find(source).equals(ds.find(dest))) {
        mst.addVertex(source);
        mst.addVertex(dest);

        mst.setEdgeWeight(mst.addEdge(source, dest), graph.getEdgeWeight(edge));
        ds.union(source, dest); // O(log N)
      }
    }

    if (ds.size() == 0) {
      var unique = graph.vertexSet().stream().findFirst(); // O(1)
      mst.addVertex(unique.get());
      for (String only : mst.vertexSet()) {
        System.out.println(only);
      }
      return;
    }

    String key = getMaxKey(graph, mst, ds); // O(N^2)

    printAnswer(mst, ds, key);
  }

  private String getMaxKey(Graph<String, DefaultWeightedEdge> graph, Graph<String, DefaultWeightedEdge> mst, UnionFindImpl<String> ds) { // O(N^2)
    String key = "";
    double total = 0;
    for (String s : ds.getSetMap().keySet()) { // O (N^2)
      var auxTotal = mst.incomingEdgesOf(s).stream()
              .map(graph::getEdgeWeight) // O(N)
              .reduce(Double::sum); // O(N)
      auxTotal.ifPresent(integer -> {

      });
      if (auxTotal.get() > total) {
        total = auxTotal.get();
        key = s;
      }
    }
    return key;
  }

  private Graph<String, DefaultWeightedEdge> getGraphResult(Graph<String, DefaultWeightedEdge> mst, UnionFindImpl<String> ds, String key) { // O(N*M)
    Graph<String, DefaultWeightedEdge> result = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
    Map<String, Set<String>> setMap = ds.getSetMap();
    for (String dest : setMap.get(key)) { // O(N)
      result.addVertex(dest);
    }
    for (String dest : setMap.get(key)) { // O(N)
      Set<DefaultWeightedEdge> weights = mst.incomingEdgesOf(dest); // O(M)

      for (DefaultWeightedEdge weight : weights) { // O(M)
        String src = mst.getEdgeSource(weight);
        String dst = mst.getEdgeTarget(weight);
        var w = mst.getEdgeWeight(weight);

        var lastEdge = result.getEdge(src, dest);
        if (!src.equals(dest) && lastEdge == null) {
          var newWeight = result.addEdge(src, dst);
          result.setEdgeWeight(newWeight, w);
        }
      }
    }
    return result;
  }

  private void printAnswer(Graph<String, DefaultWeightedEdge> mst, UnionFindImpl<String> ds, String key) {
    var result = getGraphResult(mst, ds, key); // (N*M)
    String vertexes = String.join(" ", result.vertexSet());
    System.out.println(vertexes);

    String edges = result.edgeSet().stream()
            .map(e -> {
              String source = result.getEdgeSource(e);
              String dest = result.getEdgeTarget(e);
              int weight = (int) result.getEdgeWeight(e);
              return source + " " + dest + " " + weight;
            })
            .collect(Collectors.joining("\n"));
    System.out.println(edges);
  }
}
