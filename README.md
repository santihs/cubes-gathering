# Cube’s Gathering

Course: https://www.notion.so/Algoritmia-II-7135d0e62c374ad3b938c8b727a163ac
Due date: March 6, 2023 11:30 AM
Status: Done
Type: Practice

- [x]  Implement Graph
- [x]  Implement Kruskal
- [x]  Implement class gathering
    - [x]  implement method to find by x
    - [x]  Implement method to find by m

# Instructions

Instructions

1. Implement the necessary code in order to solve the problem.
    - [x]  Make sure that you apply greedy algorithms to your solution.
    - [x]  Do not forget to write clean code.
2. Write a brief explanation of why you have chosen the greedy algorithm to solve the
problem.
    - [x]  If you use a new algorithm, include the correctness proof; otherwise, you only need
    to mention the algorithm’s name.
    
    ## There are 2 Kruskal the first one is:
    
    ### KruskalX
    
    This algorithm only suffer 4 modifications:
    
    - Will only be added to new graph the nodes and edges with a weight greater than **x**
    - If the Disjoint set is empty the result will be the first element in the new graph
    - Then, is searched in the new graph, the most greatest edges. Its time time complexity is $O(n^2)$
    - With this new edges we create the new graph that will be the answer, it has time complexity $O(n*m)$
    - Finally the total time complexity is $O(n(n+m))$
    
    ### KruskalM
    
    This algorithm only suffer 2 modifications:
    
    - Will only be united while the total of sets in the Disjoint set are greater than **m**
    - When the total of sets in the Disjoint set are m, will be print the result
    - Finally the total time complexity is $O(n*log (n))$
    
    ### Clarifications
    
    - It is used a library called **jgrapht** to the graphs and disjoint set, but the kruskol is an implemented algorithm
    - The answers are printed in **output.txt** and the input is **input.txt**
    - If you can change the txt, change the **PATH**
3. Identify the time complexity of your solution
Submission
    - [x]  For the code, you can submit your code to teams or submit it to your repository.
    - [x]  For the other points, you can submit your answers in a pdf.